package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication

@RestController
@RequestMapping("/greeting")

public class DiscussionApplication {
	ArrayList<Student> students=new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}
	public static class Student{
		String id;
		String course;
		String name;

		public Student(String id, String course, String name) {
			this.id = id;
			this.course = course;
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getCourse() {
			return course;
		}

		public void setCourse(String course) {
			this.course = course;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

	@GetMapping("/welcome")
	public String welcome(@RequestParam("user") String user, @RequestParam("role") String role) {
		if (role.equals("teacher")) {
			return String.format("Thank you for Logging in Teacher, %s", user);
		} else if (role.equals("admin")) {
			return String.format("Welcome back to the class portal Admin, %s", user);
		} else if (role.equals("student")) {
			return String.format("Welcome to the class portal, %s", user);
		} else {
			return String.format("Role is out of Range!");
		}
	}

	@GetMapping("/register")
	public String register(@RequestParam("id") String id, @RequestParam("name")String name, @RequestParam("course")String course) {
		Student s=new Student(id,course,name);
		students.add(s);
		return String.format("%s your id number is registered on the system", id);
	}

	@GetMapping("/account/{id}")
	public String account(@PathVariable("id")String id){
		int i=0;
		Student studentFound=null;
		for (Student student : students) {
			if (student.getId().equals(id)) {
				studentFound=student;
				break;
			}
		}
		if(studentFound!=null){
			return String.format("Welcome back %s! You are currently enrolled in %s",studentFound.getName(),studentFound.getCourse());
		}
		else{
			return String.format("Your provided %s is not found in the system!",id);
		}
	}


}
